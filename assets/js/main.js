const btnMinus = document.querySelector("#btn-minus");
const btnPlus = document.querySelector("#btn-plus");
const productInput = document.querySelectorAll("input");
const btnAdd = document.querySelectorAll("#btn-add");
const ul = document.querySelector("#cart_group-list");
const list = document.querySelector(".products");
const products = document.querySelectorAll(".cart_group-right-content");
const cartTotalText = document.querySelector(".cart_group_text-items span");
let buttons = document.querySelectorAll(".cart_group-right-items");
var grandTotal = "";
let btnRemove = document.querySelectorAll("#cart_group-list");
let qty = document.querySelectorAll(".cart_group-right-qty");
let qtyProduct1 = 500;
let qtyProduct2 = 50;
let qtyProduct3 = 100;
let qtyProduct4 = 200;
let totalPriceList = "";
let totalPriceList1 = "";
let totalPriceList2 = "";
let totalPriceList3 = "";
let totalPriceList4 = "";
let newQtyProduct1 = "";
let newQtyProduct2 = "";
let newQtyProduct3 = "";
let newQtyProduct4 = "";
let transTotal = cartTotalText;
// let productPrice = document.querySelectorAll(".price_value");
// console.log(cartTotalText);

const priceValue = [
  { price: 600 },
  { price: 400 },
  { price: 300 },
  { price: 700 },
];
products[0].children[2].firstElementChild.childNodes[1].textContent =
  priceValue[0].price.toFixed(2);
products[0].children[2].firstElementChild.childNodes[1].value =
  priceValue[0].price.toFixed(2);

products[1].children[2].firstElementChild.childNodes[1].textContent =
  priceValue[1].price.toFixed(2);
products[1].children[2].firstElementChild.childNodes[1].value =
  priceValue[1].price.toFixed(2);

products[2].children[2].firstElementChild.childNodes[1].textContent =
  priceValue[2].price.toFixed(2);
products[2].children[2].firstElementChild.childNodes[1].value =
  priceValue[2].price.toFixed(2);

products[3].children[2].firstElementChild.childNodes[1].textContent =
  priceValue[3].price.toFixed(2);
products[3].children[2].firstElementChild.childNodes[1].value =
  priceValue[3].price.toFixed(2);

// qty adjust
qty[0].childNodes[0].textContent = qtyProduct1 - productInput[0].value;
qty[1].childNodes[0].textContent = qtyProduct2 - productInput[1].value;
qty[2].childNodes[0].textContent = qtyProduct3 - productInput[2].value;
qty[3].childNodes[0].textContent = qtyProduct4 - productInput[3].value;

// Function for Add Buttons
const generateList = (list) => {
  // console.log(
  //   list.target.nextSibling.parentElement.children[2].children[0].children[0]
  //     .value
  // );
  let newPriceValue =
    list.target.nextSibling.parentElement.children[2].children[0].children[0]
      .value;
  if (newPriceValue === priceValue[0].price.toFixed(2)) {
    // transTotal.textContent =
    totalPriceList =
      priceValue[0].price *
      list.target.nextSibling.parentElement.childNodes[7].childNodes[3].value;
    totalPriceList1 = totalPriceList;
    // console.log(totalPriceList);
  } else if (newPriceValue === priceValue[1].price.toFixed(2)) {
    // transTotal.textContent =
    totalPriceList =
      priceValue[1].price *
      list.target.nextSibling.parentElement.childNodes[7].childNodes[3].value;
    totalPriceList2 = totalPriceList;
  } else if (newPriceValue === priceValue[2].price.toFixed(2)) {
    totalPriceList =
      priceValue[2].price *
      list.target.nextSibling.parentElement.childNodes[7].childNodes[3].value;
    totalPriceList3 = totalPriceList;
  } else if (newPriceValue === priceValue[3].price.toFixed(2)) {
    totalPriceList =
      priceValue[3].price *
      list.target.nextSibling.parentElement.childNodes[7].childNodes[3].value;
    totalPriceList4 = totalPriceList;
  }
  const html = `
  <li style="margin: 5px 0; text-align: center;" class="transaction-list">
    <div class="trans-items">${list.target.nextSibling.parentElement.childNodes[3].textContent}</div>
    <div class="trans-items">${list.target.nextSibling.parentElement.childNodes[7].childNodes[3].value}</div>
    <div class="trans-items">${list.target.nextSibling.parentElement.childNodes[5].childNodes[1].textContent}</div>
    <div class="trans-items total_item-price">${totalPriceList}</div>
   <div><button class="remove">remove</button></div>
  </li>
  `;
  ul.innerHTML += html;
  // let transTotal = cartTotalText;
  transTotal.value = Number(
    totalPriceList1 + totalPriceList2 + totalPriceList3 + totalPriceList4
  ).toFixed(2);
  transTotal.textContent = transTotal.value;
};

// Add Buttons
list.addEventListener("click", (e) => {
  e.preventDefault();
  // console.log(e.target);
  if (e.target.id) {
    generateList(e);
    e.target.setAttribute("disabled", "");
    // e.target.nextSibling.parentElement.children[3].children.setAttribute(
    //   "disabled",
    //   ""
    // );
    e.target.nextSibling.parentElement.children[3].children[0].setAttribute(
      "disabled",
      ""
    );
    e.target.nextSibling.parentElement.children[3].children[1].setAttribute(
      "disabled",
      ""
    );
    e.target.nextSibling.parentElement.children[3].children[2].setAttribute(
      "disabled",
      ""
    );
    // console.log(e.target.nextSibling.parentElement.children[3].children[0].setAttribute('disabled', ""));
  }
});

// Remove Transaction List Item
ul.addEventListener("click", (e) => {
  e.preventDefault();
  // console.log(transTotal.value);
  if (e.target.classList.contains("remove")) {
    e.target.parentElement.parentElement.remove();
    // console.log(e.target.parentElement.parentElement.children[3]);
    if (
      Number(e.target.parentElement.parentElement.children[3].textContent) ===
      totalPriceList1
    ) {
      transTotal.value = Number(transTotal.value - totalPriceList1).toFixed(2);
      transTotal.textContent = transTotal.value;
      btnAdd[0].removeAttribute("disabled");
      totalPriceList1 = 0;
      productInput[0].value = 1;
      qty[0].childNodes[0].textContent = qtyProduct1 - productInput[0].value;
      // console.log(buttons);
      buttons[0].children[0].removeAttribute("disabled");
      buttons[0].children[1].removeAttribute("disabled");
      buttons[0].children[2].removeAttribute("disabled");
    } else if (
      Number(e.target.parentElement.parentElement.children[3].textContent) ===
      totalPriceList2
    ) {
      transTotal.value = Number(transTotal.value - totalPriceList2).toFixed(2);
      transTotal.textContent = transTotal.value;
      btnAdd[1].removeAttribute("disabled");
      totalPriceList2 = 0;
      productInput[1].value = 1;
      qty[1].childNodes[0].textContent = qtyProduct2 - productInput[1].value;
      buttons[1].children[0].removeAttribute("disabled");
      buttons[1].children[1].removeAttribute("disabled");
      buttons[1].children[2].removeAttribute("disabled");
    } else if (
      Number(e.target.parentElement.parentElement.children[3].textContent) ===
      totalPriceList3
    ) {
      transTotal.value = Number(transTotal.value - totalPriceList3).toFixed(2);
      transTotal.textContent = transTotal.value;
      btnAdd[2].removeAttribute("disabled");
      totalPriceList3 = 0;
      productInput[2].value = 1;
      qty[2].childNodes[0].textContent = qtyProduct3 - productInput[2].value;
      buttons[2].children[0].removeAttribute("disabled");
      buttons[2].children[1].removeAttribute("disabled");
      buttons[2].children[2].removeAttribute("disabled");
    } else if (
      Number(e.target.parentElement.parentElement.children[3].textContent) ===
      totalPriceList4
    ) {
      transTotal.value = Number(transTotal.value - totalPriceList4).toFixed(2);
      transTotal.textContent = transTotal.value;
      btnAdd[3].removeAttribute("disabled");
      totalPriceList4 = 0;
      productInput[3].value = 1;
      qty[3].childNodes[0].textContent = qtyProduct4 - productInput[3].value;
      buttons[3].children[0].removeAttribute("disabled");
      buttons[3].children[1].removeAttribute("disabled");
      buttons[3].children[2].removeAttribute("disabled");
    }
  }
});

// Button Minus
buttons[0].childNodes[1].addEventListener("click", (e) => {
  if ((buttons[0].childNodes[1].nextSibling = productInput[0].id)) {
    if (productInput[0].value <= 0) {
      btnAdd[0].setAttribute("disabled", "");
      productInput[0].value = 0;
    }
    productInput[0].value--;
    let newNum = Number(newQtyProduct1++);
    qty[0].childNodes[0].textContent = newNum + 1;
    console.log(newQtyProduct1);
  }
});

buttons[1].childNodes[1].addEventListener("click", (e) => {
  if ((buttons[1].childNodes[1].nextSibling = productInput[1].id)) {
    if (productInput[1].value <= 0) {
      btnAdd[1].setAttribute("disabled", "");
      productInput[1].value = 0;
    }
    productInput[1].value--;
    let newNum1 = Number(newQtyProduct2++);
    qty[1].childNodes[0].textContent = newNum1 + 1;
  }
});
buttons[2].childNodes[1].addEventListener("click", (e) => {
  if ((buttons[2].childNodes[1].nextSibling = productInput[2].id)) {
    if (productInput[2].value <= 0) {
      btnAdd[2].setAttribute("disabled", "");
      productInput[2].value = 0;
    }
    productInput[2].value--;
    let newNum2 = Number(newQtyProduct3++);
    qty[2].childNodes[0].textContent = newNum2 + 1;
  }
});
buttons[3].childNodes[1].addEventListener("click", (e) => {
  if ((buttons[3].childNodes[1].nextSibling = productInput[3].id)) {
    if (productInput[3].value <= 0) {
      btnAdd[3].setAttribute("disabled", "");
      productInput[3].value = 0;
    }
    productInput[3].value--;
    let newNum3 = Number(newQtyProduct4++);
    qty[3].childNodes[0].textContent = newNum3 + 1;
  }
});

// Button Plus
buttons[0].childNodes[5].addEventListener("click", (e) => {
  // console.log(e.target);
  if ((buttons[0].childNodes[1].nextSibling = productInput[0].id)) {
    if (productInput[0].value) {
      btnAdd[0].removeAttribute("disabled");
    }
    productInput[0].value++;
    newQtyProduct1 = qtyProduct1 - productInput[0].value;
    qty[0].childNodes[0].textContent = newQtyProduct1;
  }
});

buttons[1].childNodes[5].addEventListener("click", (e) => {
  // console.log(e.target);
  if ((buttons[1].childNodes[1].nextSibling = productInput[1].id)) {
    if (productInput[1].value) {
      btnAdd[1].removeAttribute("disabled");
    }
    productInput[1].value++;
    newQtyProduct2 = qtyProduct2 - productInput[1].value;
    qty[1].childNodes[0].textContent = newQtyProduct2;
  }
});

buttons[2].childNodes[5].addEventListener("click", (e) => {
  // console.log(e.target);
  if ((buttons[2].childNodes[1].nextSibling = productInput[2].id)) {
    if (productInput[2].value) {
      btnAdd[2].removeAttribute("disabled");
    }
    productInput[2].value++;
    newQtyProduct3 = qtyProduct3 - productInput[2].value;
    qty[2].childNodes[0].textContent = newQtyProduct3;
  }
});

buttons[3].childNodes[5].addEventListener("click", (e) => {
  // console.log(e.target);
  if ((buttons[3].childNodes[1].nextSibling = productInput[3].id)) {
    if (productInput[3].value) {
      btnAdd[3].removeAttribute("disabled");
    }
    productInput[3].value++;
    newQtyProduct4 = qtyProduct4 - productInput[3].value;
    qty[3].childNodes[0].textContent = newQtyProduct4;
  }
});
